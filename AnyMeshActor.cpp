// Copyright 2020 2Bit Studios, All Rights Reserved.
// Provided by Andrew Joy to UnrealFest ANZ 2024.

#include "Foundation/AnyMeshActor.h"

#include "Components/StaticMeshComponent.h"
#include "Chaos/ImplicitObject.h"
#include "Engine/AssetManager.h"
#include "Engine/SCS_Node.h"
#include "StaticMeshCompiler.h"

#include "Foundation/PocketWorld.h"

// For some reason this member function on USCS_Node is editor only when it doesn't have to be, so copy it here
static USceneComponent* HACK_GetParentComponentTemplate(USCS_Node* This, UBlueprintGeneratedClass* BPGC)
{
    USceneComponent* ParentComponentTemplate = nullptr;
	if(This->ParentComponentOrVariableName != NAME_None)
	{
		// If the parent component template is found in the 'Components' array of the CDO (i.e. native)
		if(This->bIsParentComponentNative)
		{
			// Access the Blueprint CDO
			AActor* CDO = BPGC->GetDefaultObject<AActor>();
			if(CDO != nullptr)
			{
				// Find the component template in the CDO that matches the specified name
				for (UActorComponent* ActorComp : CDO->GetComponents())
				{
					USceneComponent* CompTemplate = Cast<USceneComponent>(ActorComp);
					if (CompTemplate && CompTemplate->GetFName() == This->ParentComponentOrVariableName)
					{
						// Found a match; this is our parent, we're done
						ParentComponentTemplate = CompTemplate;
						break;
					}
				}
			}
		}
		// Otherwise the parent component template is found in a parent Blueprint's SCS tree (i.e. non-native)
		else
		{
			// Get the Blueprint hierarchy
			TArray<UBlueprintGeneratedClass*> ParentBPStack;
			UBlueprint::GetBlueprintHierarchyFromClass(BPGC, ParentBPStack);

			// Find the parent Blueprint in the hierarchy
			for (int32 StackIndex = ParentBPStack.Num() - 1; StackIndex > 0 && !ParentComponentTemplate; --StackIndex)
			{
				UBlueprintGeneratedClass* ParentBPGC = ParentBPStack[StackIndex];
				if (ParentBPGC
					&& ParentBPGC->SimpleConstructionScript
					&& ParentBPGC->GetFName() == This->ParentComponentOwnerClassName)
				{
					// Find the SCS node with a variable name that matches the specified name
					for (USCS_Node* Node : ParentBPGC->SimpleConstructionScript->GetAllNodes())
					{
						USceneComponent* CompTemplate = Cast<USceneComponent>(Node->ComponentTemplate);
						if (CompTemplate && Node->GetVariableName() == Node->ParentComponentOrVariableName)
						{
							// Found a match; this is our parent, we're done
							ParentComponentTemplate = Cast<USceneComponent>(Node->GetActualComponentTemplate(Cast<UBlueprintGeneratedClass>(BPGC)));
							break;
						}
					}
				}
			}
		}
	}

	return ParentComponentTemplate;
};

void UStaticMeshComponentEx::RefreshHasSimpleCollision()
{
    this->bHasSimpleCollision = false;

    FPhysicsCommand::ExecuteRead(this->BodyInstance.ActorHandle, [this](FPhysicsActorHandle const& Actor)
        {
            PhysicsInterfaceTypes::FInlineShapeArray TargetShapes;
            FillInlineShapeArray_AssumesLocked(TargetShapes, Actor);

            for (FPhysicsShapeHandle const& Shape : TargetShapes)
            {
#ifdef WITH_CHAOS
                if (!Shape.GetGeometry().IsConvex())
                {
                    continue;
                }
#else
                ECollisionShapeType ShapeType = FPhysicsInterface::GetShapeType(Shape);
                if(ShapeType == ECollisionShapeType::Heightfield || ShapeType == ECollisionShapeType::Trimesh)
                {
                    continue;
                }
#endif

                this->bHasSimpleCollision = true;

                return;
            }
        });
}

void UStaticMeshComponentEx::PostInitProperties()
{
    Super::PostInitProperties();

    this->RefreshHasSimpleCollision();
}

bool UStaticMeshComponentEx::SetStaticMesh(UStaticMesh* NewMesh)
{
    if (this->GetStaticMesh() != NewMesh)
    {
        bool const Result = Super::SetStaticMesh(NewMesh);

        this->RefreshHasSimpleCollision();

        return Result;
    }

    return false;
}

bool UStaticMeshComponentEx::UpdateOverlapsImpl(TOverlapArrayView const* PendingOverlaps, bool bDoNotifies, TOverlapArrayView const* OverlapsAtEndLocation)
{
    if (this->bHasSimpleCollision)
    {
        return Super::UpdateOverlapsImpl(PendingOverlaps, bDoNotifies, OverlapsAtEndLocation);
    }
    else
    {
        bool bCanSkipUpdateOverlaps = true;

        if (this->GetGenerateOverlapEvents() && this->IsQueryCollisionEnabled())
        {
            bCanSkipUpdateOverlaps = false;

            // don't check PendingOverlaps or OverlapsAtEndLocation, they must be empty with no simple collision

            TArray<FOverlapResult> Overlaps;
            if (this->GetStaticMesh() != nullptr)
            {
                FBoxSphereBounds const MeshBounds = this->GetStaticMesh()->GetBounds();

                FCollisionQueryParams QueryParams = FCollisionQueryParams::DefaultQueryParam;
                QueryParams.AddIgnoredActor(this->GetOwner());
                QueryParams.bIgnoreBlocks = true;

                // Query bounding box
                this->GetWorld()->OverlapMultiByChannel(
                    Overlaps,
                    this->GetComponentLocation() + this->GetComponentQuat().RotateVector(MeshBounds.Origin),
                    this->GetComponentQuat(),
                    this->GetCollisionObjectType(),
                    FCollisionShape::MakeBox(MeshBounds.BoxExtent),
                    QueryParams,
                    this->GetCollisionResponseToChannels());

                for (int i = 0; i < Overlaps.Num(); ++i)
                {
                    if (this->ComponentOverlapComponent(
                        Overlaps[i].Component.Get(),
                        Overlaps[i].Component->GetComponentLocation(),
                        Overlaps[i].Component->GetComponentQuat(),
                        FCollisionQueryParams::DefaultQueryParam))
                    {
                        // New or existing overlap, attempt to begin
                        this->BeginComponentOverlap(FOverlapInfo(Overlaps[i].Component.Get()), bDoNotifies);
                    }
                    else
                    {
                        // No overlap, discard
                        Overlaps.RemoveAtSwap(i);

                        --i;
                    }
                }
            }

            for (int i = 0; i < this->OverlappingComponents.Num(); ++i)
            {
                int32 const OverlapsIndex = Overlaps.IndexOfByPredicate(
                [OldOverlap = this->OverlappingComponents[i]](FOverlapResult const& NewOverlap)
                    {
                        return OldOverlap.OverlapInfo.Component == NewOverlap.Component;
                    });

                if (OverlapsIndex < 0)
                {
                    // No longer overlapping, attempt to end
                    this->EndComponentOverlap(this->OverlappingComponents[i], bDoNotifies);
                }
                else
                {
                    // Still overlapping, remove from further checks
                    Overlaps.RemoveAtSwap(OverlapsIndex);
                }
            }
        }

        TInlineComponentArray<USceneComponent*> AttachedChildren;
        AttachedChildren.Append(this->GetAttachChildren());

        for (USceneComponent* const AttachedChild : AttachedChildren)
        {
            if (AttachedChild != nullptr)
            {
                // Do not pass on OverlapsAtEndLocation, it only applied to this component.
                bCanSkipUpdateOverlaps &= AttachedChild->UpdateOverlaps(nullptr, bDoNotifies, nullptr);
            }
        }

        // Update physics volume using most current overlaps
        if (this->GetShouldUpdatePhysicsVolume())
        {
            this->UpdatePhysicsVolume(bDoNotifies);

            bCanSkipUpdateOverlaps = false;
        }

        return bCanSkipUpdateOverlaps;
    }
}

template <typename TMeshComponent>
TMeshComponent* CreateMesh(UObject* Outer, UClass* Class = TMeshComponent::StaticClass())
{
    if (FUObjectThreadContext::Get().IsInConstructor)
    {
        // TODO: Remove when we can rename or retype CDO's, so that skeletal/static/spline could share a name
        static auto const MakeReadableName = [](FString const& In)
        {
            FString Result = In;
            Result.RemoveFromEnd("Ex");
            Result.RemoveFromEnd("Component");

            TArray<TCHAR>& Characters = Result.GetCharArray();
            for (int i = 0; i < Characters.Num(); ++i)
            {
                if (FChar::IsUpper(Characters[i]))
                {
                    if (i > 0 && FChar::IsAlnum(Characters[i - 1]))
                    {
                        Characters.Insert(' ', i);

                        ++i;
                    }
                }
                else if (FChar::IsDigit(Characters[i]))
                {
                    if (i > 0 && FChar::IsAlnum(Characters[i - 1]) && !FChar::IsDigit(Characters[i - 1]))
                    {
                        Characters.Insert(' ', i);

                        ++i;
                    }
                }
                else if (FChar::IsUnderscore(Characters[i]))
                {
                    Characters[i] = ' ';
                }
            }

            return Result;
        };

        return (TMeshComponent*)Outer->CreateDefaultSubobject(
            *MakeReadableName(Class->GetName()),
            TMeshComponent::StaticClass(),
            Class,
            true,
            false);
    }
    else
    {
        return NewObject<TMeshComponent>(Outer, Class);
    }
}

UMeshComponent* AAnyMeshActor::SpawnMesh(USceneComponent* Parent, UStreamableRenderAsset* NewMesh)
{
    UMeshComponent* ReturnValue = nullptr;
    if (USkeletalMesh* const NewSkeletalMesh = Cast<USkeletalMesh>(NewMesh))
    {
        USkeletalMeshComponent* const SkeletalMeshComponent = CreateMesh<USkeletalMeshComponent>(Parent);

        SkeletalMeshComponent->SetAnimationMode(EAnimationMode::AnimationSingleNode);
        SkeletalMeshComponent->SetSkeletalMesh(NewSkeletalMesh);

        ReturnValue = SkeletalMeshComponent;
    }
    else
    {
        UStaticMesh* const NewStaticMesh = Cast<UStaticMesh>(NewMesh);

        UStaticMeshComponent* const StaticMeshComponent = CreateMesh<UStaticMeshComponentEx>(Parent);

        StaticMeshComponent->SetStaticMesh(NewStaticMesh);

        ReturnValue = StaticMeshComponent;
    }

    ReturnValue->SetReceivesDecals(false);
    ReturnValue->AttachToComponent(Parent, FAttachmentTransformRules::SnapToTargetIncludingScale);

    if (Parent->IsRegistered())
    {
        ReturnValue->RegisterComponent();
    }

    return ReturnValue;
}

AAnyMeshActor::AAnyMeshActor()
    :AAnyMeshActor(USceneComponent::StaticClass())
{
}

AAnyMeshActor::AAnyMeshActor(TSubclassOf<USceneComponent> const& RootClass)
{
    this->Root = (USceneComponent*)this->CreateDefaultSubobject("Root", USceneComponent::StaticClass(), RootClass, true, false);
    this->RootComponent = this->Root;

    this->SetMesh(nullptr);
    this->Mesh->SetReceivesDecals(false);
    this->Mesh->SetupAttachment(this->Root);
}

USceneComponent* AAnyMeshActor::GetDefaultAttachComponent() const
{
    return this->GetMesh();
}

UStreamableRenderAsset* AAnyMeshActor::GetMeshAsset() const
{
    if (UStaticMeshComponent const* const StaticMesh = this->GetStaticMesh())
    {
        return StaticMesh->GetStaticMesh();
    }
    else if (USkeletalMeshComponent const* const SkeletalMesh = this->GetSkeletalMesh())
    {
        return SkeletalMesh->GetSkeletalMeshAsset();
    }

    return nullptr;
}

UStaticMesh* AAnyMeshActor::GetStaticMeshAsset() const
{
    if (UStaticMeshComponent const* const StaticMesh = this->GetStaticMesh())
    {
        return StaticMesh->GetStaticMesh();
    }

    return nullptr;
}

USkeletalMesh* AAnyMeshActor::GetSkeletalMeshAsset() const
{
    if (USkeletalMeshComponent const* const SkeletalMesh = this->GetSkeletalMesh())
    {
        return SkeletalMesh->GetSkeletalMeshAsset();
    }

    return nullptr;
}

void AAnyMeshActor::SetMesh(UStreamableRenderAsset* NewMesh, UClass* OverrideClass)
{
    UMeshComponent* const OldMeshComponent = this->Mesh;
    UMeshComponent* NewMeshComponent = nullptr;

    // Perform skeletal cast ahead of if statement, because the condition is quite complex
    USkeletalMesh* const NewSkeletalMesh = Cast<USkeletalMesh>(NewMesh);
    bool IsOverrideSkeletal = false;
    if (OverrideClass != nullptr && (NewSkeletalMesh != nullptr || NewMesh == nullptr))
    {
        IsOverrideSkeletal = OverrideClass->IsChildOf(USkeletalMeshComponent::StaticClass());
    }

    // Prefer to persist current mesh class if not overridden
    if (OverrideClass == nullptr && OldMeshComponent != nullptr)
    {
        OverrideClass = OldMeshComponent->GetClass();
    }

    if (NewSkeletalMesh || IsOverrideSkeletal)
    {
        UClass* const SkeletalMeshComponentClass =
            OverrideClass != nullptr && IsOverrideSkeletal
            ? OverrideClass
            : USkeletalMeshComponent::StaticClass();

        USkeletalMeshComponent* SkeletalMeshComponent = nullptr;
        if (OldMeshComponent != nullptr && OldMeshComponent->IsA(SkeletalMeshComponentClass))
        {
            SkeletalMeshComponent = (USkeletalMeshComponent*)OldMeshComponent;
        }
        else
        {
            SkeletalMeshComponent = CreateMesh<USkeletalMeshComponent>(this, SkeletalMeshComponentClass);
            SkeletalMeshComponent->SetAnimationMode(EAnimationMode::AnimationSingleNode);

            NewMeshComponent = SkeletalMeshComponent;
        }

        SkeletalMeshComponent->SetSkeletalMesh(NewSkeletalMesh);
    }
    else
    {
        UStaticMesh* const NewStaticMesh = Cast<UStaticMesh>(NewMesh);

        UClass* const StaticMeshComponentClass =
            OverrideClass != nullptr && OverrideClass->IsChildOf(UStaticMeshComponent::StaticClass())
            ? OverrideClass
            : UStaticMeshComponentEx::StaticClass();

        UStaticMeshComponent* StaticMeshComponent = nullptr;
        if (OldMeshComponent != nullptr && OldMeshComponent->IsA(StaticMeshComponentClass))
        {
            StaticMeshComponent = (UStaticMeshComponent*)OldMeshComponent;
        }
        else
        {
            StaticMeshComponent = CreateMesh<UStaticMeshComponent>(this, StaticMeshComponentClass);

            NewMeshComponent = StaticMeshComponent;
        }

        StaticMeshComponent->SetStaticMesh(NewStaticMesh);
    }

    if (NewMeshComponent != nullptr)
    {
        NewMeshComponent->SetMobility(EComponentMobility::Movable);

        if (OldMeshComponent != nullptr)
        {
            NewMeshComponent->AttachToComponent(
                OldMeshComponent->GetAttachParent(),
                FAttachmentTransformRules::KeepRelativeTransform,
                OldMeshComponent->GetAttachSocketName());

            NewMeshComponent->SetRelativeTransform(OldMeshComponent->GetRelativeTransform());

            if (OldMeshComponent->IsRegistered())
            {
                NewMeshComponent->RegisterComponent();
            }

            UObject const* const OldMeshCDO = OldMeshComponent->GetClass()->GetDefaultObject();

            // Copy all UMeshComponent properties (not child classes), if changed from static/skeletal child class defaults
            // TODO: Copy is not stress tested (see Class.cpp::SerializeVersionedTaggedProperties line 1541), especially for arrays and fastarrays
            for (FProperty const* Property = UMeshComponent::StaticClass()->PropertyLink; Property; Property = Property->PropertyLinkNext)
            {
                uint8 const* const DefaultData = Property->ContainerPtrToValuePtr<uint8>(OldMeshCDO);
                uint8 const* const SrcData = Property->ContainerPtrToValuePtr<uint8>(OldMeshComponent);
                if (!Property->Identical(DefaultData, SrcData))
                {
                    uint8* const DestData = Property->ContainerPtrToValuePtr<uint8>(NewMeshComponent);

                    Property->CopyCompleteValue(DestData, SrcData);
                }
            }

            TArray<USceneComponent*> MeshChildren;
            if (FUObjectThreadContext::Get().IsInConstructor)
            {
                ForEachObjectWithOuter(this, [OldMeshComponent, &MeshChildren](UObject* Object)
                {
                    if (Object->IsDefaultSubobject())
                    {
                        if (USceneComponent* const ObjectComponent = Cast<USceneComponent>(Object))
                        {
                            if (ObjectComponent->GetAttachParent() == OldMeshComponent)
                            {
                                MeshChildren.AddUnique(ObjectComponent);
                            }
                        }
                    }
                }, false);
            }
            else
            {
                OldMeshComponent->GetChildrenComponents(false, MeshChildren);
            }

            for (USceneComponent* const MeshChild : MeshChildren)
            {
                FTransform const PreviousRelativeTransform = MeshChild->GetRelativeTransform();
                FName const PreviousSocket = MeshChild->GetAttachSocketName();

                MeshChild->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
                MeshChild->AttachToComponent(NewMeshComponent,
                    FAttachmentTransformRules::KeepRelativeTransform,
                    PreviousSocket);

                MeshChild->SetRelativeTransform(PreviousRelativeTransform);
            }

            OldMeshComponent->DestroyComponent();
        }

        this->Mesh = NewMeshComponent;
    }

    // If post-init, manually update transforms and overlaps
    if (this->HasActorBegunPlay())
    {
        this->Mesh->UpdateChildTransforms();

        this->Mesh->UpdateOverlaps();
    }
}

void AAnyMeshActor::SetMesh(TSoftObjectPtr<UStreamableRenderAsset> const& NewMesh, bool AllowAsync, TSubclassOf<UMeshComponent> OverrideClass)
{
    // If a preview world (i.e. icon generator), load immediately
    AllowAsync = AllowAsync && !UPocketWorld::Contains(this);

    if (!AllowAsync)
    {
        UStreamableRenderAsset* const LoadedMesh = NewMesh.LoadSynchronous();

#if WITH_EDITOR
        if (UStaticMesh* const LoadedStaticMesh = Cast<UStaticMesh>(LoadedMesh))
        {
            FStaticMeshCompilingManager::Get().FinishCompilation({ LoadedStaticMesh });
        }
#endif
    }

    if (NewMesh.IsValid())
    {
        this->SetMesh(NewMesh.Get(), OverrideClass);
    }
    else if (NewMesh.IsPending())
    {
        TArray<FSoftObjectPath> AssetsToLoad;
        AssetsToLoad.Add(NewMesh.ToSoftObjectPath());

        if (AssetsToLoad.Num() > 0)
        {
            UAssetManager::Get().LoadAssetList(AssetsToLoad, FStreamableDelegate::CreateWeakLambda(
                this,
                [this, NewMesh, OverrideClass]()
                {
                    this->SetMesh(NewMesh, false, OverrideClass);
                }));
        }
    }
    else
    {
        this->SetMesh(nullptr, OverrideClass);
    }
}

void AAnyMeshActor::CopyMesh(AActor* From)
{
    // Recursive core implementation
    // `PreGatheredChildren` allows us to manually gather CDO children as a single preprocess step
    static TFunction<void(USceneComponent*, USceneComponent*, TArray<TObjectPtr<USceneComponent>> const*)> const CopyComponents =
        [](USceneComponent* From, USceneComponent* To, TArray<TObjectPtr<USceneComponent>> const* const PreGatheredChildren = nullptr)
        {
            TArray<TObjectPtr<USceneComponent>> const* FromChildren = PreGatheredChildren;
            if (FromChildren == nullptr)
            {
                FromChildren = &From->GetAttachChildren();
            }

            for (USceneComponent* const FromChild : *FromChildren)
            {
                if (FromChild != nullptr && FromChild->GetAttachParent() == From)
                {
                    USceneComponent* ToChild = nullptr;
                    if (UMeshComponent* const FromMeshChild = Cast<UMeshComponent>(FromChild))
                    {
                        UMeshComponent* const ToMeshChild = NewObject<UMeshComponent>(
                            To,
                            FromMeshChild->GetClass(),
                            FromMeshChild->GetFName(),
                            RF_NoFlags,
                            FromChild); // Copy component properties using the UE template system

                        ToChild = ToMeshChild;
                    }
                    else
                    {
                        ToChild = NewObject<USceneComponent>(
                            To,
                            USceneComponent::StaticClass(),
                            FromChild->GetFName());
                    }

                    ToChild->AttachToComponent(To, FAttachmentTransformRules::SnapToTargetIncludingScale, FromChild->GetAttachSocketName());
                    ToChild->SetRelativeTransform(FromChild->GetRelativeTransform());
                    ToChild->RegisterComponent();

                    To->GetOwner()->AddInstanceComponent(ToChild);

                    CopyComponents(FromChild, ToChild, PreGatheredChildren);
                }
            }
        };

    // Clean up components copied by a previous call to CopyMesh()
    TArray<USceneComponent*> OldChildren;
    this->GetMesh()->GetChildrenComponents(true, OldChildren);
    for (USceneComponent* const OldChild : OldChildren)
    {
        if (this->GetInstanceComponents().Contains(OldChild))
        {
            this->RemoveInstanceComponent(OldChild);

            OldChild->DestroyComponent();
        }
    }

    USceneComponent* FromComponent = From->GetRootComponent();
    if (FromComponent != nullptr)
    {
        // If copying from a CDO, manually gather all children as a single step (and support blueprint construction script components)
        if (From->IsTemplate())
        {
            // Must be an array of TObjectPtr<> so that we can also use a pointer to GetChildrenComponents() inside of CopyComponents()
            TArray<TObjectPtr<USceneComponent>> AllFromChildren;
            for (UActorComponent* const FromActorChild : From->GetComponents())
            {
                if (USceneComponent* const FromSceneChild = Cast<USceneComponent>(FromActorChild))
                {
                    AllFromChildren.Add(FromSceneChild);
                }
            }

            // Gather blueprint components
            if (UBlueprintGeneratedClass* const FromBlueprintLeaf = Cast<UBlueprintGeneratedClass>(From->GetClass()))
            {
                TArray<UBlueprintGeneratedClass const*> FromBlueprints;
                UBlueprintGeneratedClass::GetGeneratedClassesHierarchy(FromBlueprintLeaf, FromBlueprints);

                for (UBlueprintGeneratedClass const* const FromBlueprint : FromBlueprints)
                {
                    TFunction<USceneComponent*(USCS_Node*)> const SearchNode = [&](USCS_Node* Node) -> USceneComponent*
                    {
                        if (USceneComponent* ChildComponent = Cast<USceneComponent>(
                            Node->GetActualComponentTemplate(
                                const_cast<UBlueprintGeneratedClass*>(FromBlueprint))))
                        {
                            AllFromChildren.Add(ChildComponent);

                            //if (USceneComponent* const ParentComponent = Node->GetParentComponentTemplate(FromBlueprint))
                            if (USceneComponent* const ParentComponent = HACK_GetParentComponentTemplate(Node, const_cast<UBlueprintGeneratedClass*>(FromBlueprint)))
                            {
                                ChildComponent->SetupAttachment(ParentComponent);
                            }

                            for (USCS_Node* const ChildNode : Node->ChildNodes)
                            {
                                if (USceneComponent* const NodeChildComponent = SearchNode(ChildNode))
                                {
                                    NodeChildComponent->SetupAttachment(ChildComponent);
                                }
                            }

                            return ChildComponent;
                        }

                        return nullptr;
                    };

                    if (FromBlueprint->SimpleConstructionScript)
                    {
                        for (USCS_Node* const CDONode : FromBlueprint->SimpleConstructionScript->GetRootNodes())
                        {
                            // Recursive depth-first search
                            SearchNode(CDONode);
                        }
                    }
                }
            }

            CopyComponents(FromComponent, this->RootComponent, &AllFromChildren);
        }
        else
        {
            // If we aren't copying from a CDO, skip the pre-gather step and just use GetAttachedChildren()
            CopyComponents(FromComponent, this->RootComponent, nullptr);
        }
    }
}
