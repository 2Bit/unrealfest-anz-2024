# Game Framework Extensions

[Link to Slides](https://docs.google.com/presentation/d/1IBFIm9WSlwRQp4qYR-nZOFawYsDyHzfE/edit?usp=sharing&ouid=109156759549903406244&rtpof=true&sd=true).

Written by Andrew Joy and provided to UnrealFest Gold Coast 2024. For learning purposes only.

Any questions to [andrew.joy@2bitstudios.com.au](mailto:andrew.joy@2bitstudios.com.au).
