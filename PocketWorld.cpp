// Copyright 2020 2Bit Studios, All Rights Reserved.
// Provided by Andrew Joy to UnrealFest ANZ 2024.

#include "Foundation/PocketWorld.h"

#include "Components/SkyLightComponent.h"
#include "GameFramework/GameModeBase.h"

UWorld* UPocketWorld::Create(
    TSoftObjectPtr<UWorld> Template,
    bool bSimulatesPhysics,
    bool bSupportsNavigation,
    bool bSupportsAI,
    bool bSupportsAudio)
{
    return UPocketWorld::Create(
        Template,
        FWorldInitializationValues()
            .SetTransactional(false)
            .ShouldSimulatePhysics(bSimulatesPhysics)
            .CreateNavigation(bSupportsNavigation)
            .CreateAISystem(bSupportsAI)
            .AllowAudioPlayback(bSupportsAudio)
            .SetDefaultGameMode(AGameModeBase::StaticClass()));
}

UWorld* UPocketWorld::Create(TSoftObjectPtr<UWorld> const& Template, FWorldInitializationValues const& Init)
{
    // Reverse engineered from UnrealEngine.cpp::CreatePIEWorldByLoadingFromPack

    FName const InnerPackageName = *Template.GetLongPackageName();
    FName const OuterPackageName = MakeUniqueObjectName(nullptr, UPackage::StaticClass(), InnerPackageName);

    UPackage* const OuterPackage = CreatePackage(*OuterPackageName.ToString());
    OuterPackage->SetFlags(EObjectFlags::RF_Transient);

    UWorld::WorldTypePreLoadMap.FindOrAdd(OuterPackageName) = EWorldType::GamePreview;
    UPackage* const InnerPackage = LoadPackage(OuterPackage, *InnerPackageName.ToString(), LOAD_None);
    UWorld::WorldTypePreLoadMap.Remove(OuterPackageName);

    UWorld* NewWorld = UWorld::FindWorldInPackage(InnerPackage);
    if (NewWorld == nullptr)
    {
        NewWorld = UWorld::FollowWorldRedirectorInPackage(InnerPackage);
    }

    // editor context so it's accessible via the UI
    FWorldContext& NewWorldContext = GEngine->CreateNewWorldContext(EWorldType::Editor);
    NewWorldContext.SetCurrentWorld(NewWorld);
    NewWorldContext.OwningGameInstance = NewWorld->GetGameInstance(); // after SetCurrentWorld otherwise game instance events will be called

    FWorldDelegates::OnWorldCleanup.AddWeakLambda(
        NewWorld,
        [NewWorld](UWorld* World, bool bSessionEnded, bool bCleanupResources)
        {
            if (NewWorld == World)
            {
                GEngine->DestroyWorldContext(World);
            }
        });

    // Don't worry too much about GC, worlds are a mess of references (including the PersistentLevel which has a circular reference) and we have to call DestroyWorld/Context anyway
    // World->ClearFlags(RF_Public | RF_Standalone);

    NewWorld->InitWorld(Init);

    // NewWorld->SetGameInstance(this->GetGameInstance()); // add gameinstance if a gamemode is needed, otherwise crash
    // NewWorld->SetGameMode(NewWorld->URL);

    NewWorld->InitializeActorsForPlay(NewWorld->URL);
    NewWorld->BeginPlay();
    NewWorld->bWorldWasLoadedThisTick = true;

    USkyLightComponent::UpdateSkyCaptureContents(NewWorld);

    return NewWorld;
}

bool UPocketWorld::Contains(UObject const* Object)
{
    return Object != nullptr ? Object->GetWorld()->IsPreviewWorld() : false;
}
