// Copyright 2020 2Bit Studios, All Rights Reserved.
// Provided by Andrew Joy to UnrealFest ANZ 2024.

#pragma once

#include "GameFramework/Actor.h"

#include "AnyMeshActor.generated.h"

UCLASS()
class FOUNDATION_API UStaticMeshComponentEx : public UStaticMeshComponent
{
    GENERATED_BODY()

private:
    uint8 bHasSimpleCollision : 1;

    void RefreshHasSimpleCollision();

public:
    virtual void PostInitProperties() override;
    virtual bool SetStaticMesh(class UStaticMesh* NewMesh) override;

protected:
    virtual bool UpdateOverlapsImpl(TOverlapArrayView const* PendingOverlaps = nullptr, bool bDoNotifies = true, TOverlapArrayView const* OverlapsAtEndLocation = nullptr) override;
};

/*
 * Known Issues
 * - Cannot name post-constructor skeletal or static mesh components
 *      - Would require renaming the original DSO so that NewObject works, not worth it
 * - Setting a Skeletal then a Static mesh in the construction script will leak the original Static mesh component
 *      - Unfixable, engine bug
 */
UCLASS()
class FOUNDATION_API AAnyMeshActor : public AActor
{
    GENERATED_BODY()

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<USceneComponent> Root;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UMeshComponent> Mesh;

public:
    FORCEINLINE UMeshComponent* GetMesh() const { return this->Mesh; }
    FORCEINLINE UStaticMeshComponent* GetStaticMesh() const { return Cast<UStaticMeshComponent>(this->Mesh); }
    FORCEINLINE USkeletalMeshComponent* GetSkeletalMesh() const { return Cast<USkeletalMeshComponent>(this->Mesh); }

    template <typename TMeshComponent> TMeshComponent* GetMesh() const { return Cast<TMeshComponent>(this->Mesh); }

    static UMeshComponent* SpawnMesh(USceneComponent* Parent, UStreamableRenderAsset* NewMesh);

    AAnyMeshActor();
    AAnyMeshActor(TSubclassOf<USceneComponent> const& RootClass);

public:
    virtual USceneComponent* GetDefaultAttachComponent() const override;

public:
    UFUNCTION(BlueprintCallable, Category = "Mesh")
    UStreamableRenderAsset* GetMeshAsset() const;
    
    UFUNCTION(BlueprintCallable, Category = "Mesh")
    UStaticMesh* GetStaticMeshAsset() const;

    UFUNCTION(BlueprintCallable, Category = "Mesh")
    USkeletalMesh* GetSkeletalMeshAsset() const;

    void SetMesh(UStreamableRenderAsset* NewMesh, UClass* OverrideClass = nullptr);
    void SetMesh(TObjectPtr<UStreamableRenderAsset> NewMesh, UClass* OverrideClass = nullptr) { this->SetMesh(NewMesh.Get(), OverrideClass); }

    UFUNCTION(BlueprintCallable, Category = "Mesh")
    void SetMesh(UStreamableRenderAsset* NewMesh, TSubclassOf<UMeshComponent> OverrideClass) { this->SetMesh(NewMesh, OverrideClass.Get()); }

    void SetMesh(TSoftObjectPtr<UStreamableRenderAsset> const& NewMesh, bool AllowAsync, TSubclassOf<UMeshComponent> OverrideClass = nullptr);
    
    UFUNCTION(BlueprintCallable, Category = "Mesh")
    void SetSoftMesh(TSoftObjectPtr<UStreamableRenderAsset> NewMesh, bool AllowAsync = true, TSubclassOf<UMeshComponent> OverrideClass = nullptr) { this->SetMesh(NewMesh, AllowAsync, OverrideClass); }

    UFUNCTION(BlueprintCallable, Category = "Mesh")
    void CopyMesh(AActor* From);
};
