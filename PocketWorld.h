// Copyright 2020 2Bit Studios, All Rights Reserved.
// Provided by Andrew Joy to UnrealFest ANZ 2024.

#pragma once

#include "PocketWorld.generated.h"

UCLASS(Abstract)
class FOUNDATION_API UPocketWorld final : public UObject
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, Category = "World", meta = (DisplayName = "Create Pocket World"))
    static UWorld* Create(
        TSoftObjectPtr<UWorld> Template,
        bool bSimulatesPhysics = false,
        bool bSupportsNavigation = false,
        bool bSupportsAI = false,
        bool bSupportsAudio = false);

    static UWorld* Create(TSoftObjectPtr<UWorld> const& Template, FWorldInitializationValues const& Init);

    UFUNCTION(BlueprintCallable, Category = "World", meta = (DisplayName = "Is In Pocket World", WorldContext = "Object"))
    static bool Contains(UObject const* Object);
};
